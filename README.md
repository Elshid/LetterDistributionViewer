<!--
SPDX-FileCopyrightText: 2023 Mirko Adam <mirko@four-m.de

SPDX-License-Identifier: GFDL-1.3-or-later
-->

# Letter Distribution Viewer

---

[![GPL enforced badge](https://img.shields.io/badge/GPL-enforced-blue.svg "This project enforces the GPL.")](https://gplenforced.org)
![Woodpecker badge](https://woodpecker.four-m.de/api/badges/Elshid/LetterDistributionViewer/status.svg "Status of the building process")

Just some student project that shows the distribution of the letters in a document. It also shows the entropy of the document.

## Usage

letterDistribution FILE [OPTIONS]

### Available Options

#### Verbose

By adding `-v` as last argument, you will get additional information about what is going on in the background of the program.
Normal verbose information will be printed in blue, while warnings will be printed in yellow.

Even if verbose mode is disabled, it will still print information about errors. This information is printed in red. 

### Limitations of current implementation

While this program should have full unicode support, due to the incredible size of the constantly evolving Unicode standard, this cannot be guaranteed.
However, in case of some unexpected behaviour warnings are displayed, if verbose mode is enabled.
If you do encounter such warnings, please report them as an issue.

## License

This program is Free Software! Feel Free to run, modify and share under the conditions of the AGPL!
