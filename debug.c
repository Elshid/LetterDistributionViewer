// SPDX-FileCopyrightText: 2023 Mirko Adam <mirko@four-m.de
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "debug.h"

void verbosing(const char *toBePrinted, bool checkVerbose)
{
    if (checkVerbose)
    {
        BLUE
        printf("%s", toBePrinted);
        RESET
        fflush(stderr);
        fflush(stdout);
    }
}

void severeErrorControl(const char *toBePrinted)
{
    RED
    printf("\n");
    perror(toBePrinted);
    RESET
    fflush(stderr);
    printf("\n");
    fflush(stdout);
    abort();
}

void errorControl(const char *toBePrinted)
{
    RED
    printf("\n");
    perror(toBePrinted);
    RESET
    fflush(stderr);
    printf("\n");
    fflush(stdout);
}

void warning(const char *toBePrinted, bool checkVerbose)
{
    if (checkVerbose)
    {
        YELLOW
        printf("\n");
        printf("%s", toBePrinted);
        RESET
    }
}

bool checkVerbose(int argc, char** argv)
{
    if (strcmp(argv[argc - 1], "-v") == 0)
    {
        return true;
    }
    return false;
}
