// SPDX-FileCopyrightText: 2023 Mirko Adam <mirko@four-m.de
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#ifndef DEBUG_H
#define DEBUG_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define RED printf("\033[0;31m");
#define BLUE printf("\033[0;34m");
#define YELLOW printf("\033[0;33m");
#define RESET printf("\033[1;0m");

void verbosing(const char *toBePrinted, bool checkVerbose);

void severeErrorControl(const char *toBePrinted);

void errorControl(const char *toBePrinted);

void warning(const char *toBePrinted, bool checkVerbose);

bool checkVerbose(int argc, char** argv);

#endif
