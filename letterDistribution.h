// SPDX-FileCopyrightText: 2023 Mirko Adam <mirko@four-m.de
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#ifndef LETTERDISTRIBUTION_H
#define LETTERDISTRIBUTION_H

// the defines

#define MAX_DEFAULT_SYMBOLS 128
#define MAX_DESCRIPTION_LENGTH 50
#define ABSOLUTE_FREQUENCY_LENGTH 18 // The length of the words "Absolute Frequency"
#define CHARACTER_LENGTH 10 // The length of the word "Character"


// the includes

#include "debug.h"
#include <wchar.h>
#include <wctype.h>
#include <locale.h>
#include <errno.h>
#include <math.h>
#include <sys/ioctl.h>
#include <unistd.h>

// the structs

struct letterData {
    wint_t character;
    wint_t description[MAX_DESCRIPTION_LENGTH];
    int descriptionLength;
    int absFreq;
    long double relFreq;
};

typedef struct letterData letterInfo;

struct programData {
    bool verbose;
    bool printDiagram;
    int checked;
    char* filename;
    FILE* f;
    int differentSymbols;
    int totalSymbols;
    letterInfo* charInfo;
    int maxSymbols;
    int numUnprintableCharacters;
    int maxRealDescriptionLength;
};

typedef struct programData pdata_t;

// the functions

pdata_t getParameters(int argc, char **argv);

void cleanup(pdata_t programData);

void emergencyCleanup(pdata_t programData, char *errorString);

pdata_t countingCharacters(pdata_t programData);

int compareLetterByFreq(const void *a, const void *b);

void sortCharacters(letterInfo *charInfo, int differentSymbols, bool verbose);

long double calculateEntropy(pdata_t programData);

void printCharacters(pdata_t programData, long double entropy);

pdata_t moreDifferentCharacters(pdata_t programData);

void printTableEnd(int part, int maxCharLength);

#endif
