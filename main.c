// SPDX-FileCopyrightText: 2023 Mirko Adam <mirko@four-m.de
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#define _XOPEN_SOURCE
#include "letterDistribution.h"
#include <wchar.h>

int compareLetterByFreq(const void *a, const void *b)
{
    letterInfo *letterInfoA = (letterInfo *) a;
    letterInfo *letterInfoB = (letterInfo *) b;
    return letterInfoB->absFreq - letterInfoA->absFreq;
}

void cleanup(pdata_t programData)
{
    verbosing("Returning...\n", programData.verbose);
    programData.checked = fclose(programData.f);
    if (programData.checked == EOF)
    {
        severeErrorControl("ERROR: Closing file stream failed!\n");
    }
    free(programData.filename);
    free(programData.charInfo);
}

void emergencyCleanup(pdata_t programData, char *errorString)
{
    verbosing("Returning...\n", programData.verbose);
    programData.checked = fclose(programData.f);
    if (programData.checked == EOF)
    {
        severeErrorControl("ERROR: Closing file stream failed!\n");
    }
    free(programData.filename);
    free(programData.charInfo);
    severeErrorControl(errorString);
    abort();
}

pdata_t getParameters(int argc, char **argv)
{
    pdata_t programData;
    programData.printDiagram = false;
    if (strcmp(argv[argc - 1], "-p") == 0)
    {
        programData.printDiagram = true;
        argc--;
    }
    programData.verbose = checkVerbose(argc, argv);
    if (programData.verbose)
    {
        argc--;
    }
    verbosing("Initializing variables...", programData.verbose);
    programData.checked = 0;
    programData.differentSymbols = 0;
    programData.totalSymbols = 0;
    programData.numUnprintableCharacters = 0;
    programData.maxSymbols = MAX_DEFAULT_SYMBOLS;
    programData.maxRealDescriptionLength = CHARACTER_LENGTH;
    if (argc <= 1)
    {
        severeErrorControl("ERROR: You did not give a file to analyze, returning...");
    }
    programData.filename = malloc(strlen(argv[1]) + 10 * sizeof(char));
    if(programData.filename == NULL)
    {
        severeErrorControl("ERROR: Allocating memory for filename failed!\n");
    }
    strcpy(programData.filename, argv[1]);
    programData.f = fopen(programData.filename, "r");
    if (programData.f == NULL)
    {
        errorControl("\nERROR: Could not open file!");
        free(programData.filename);
        abort();
    }
    programData.checked = fwide(programData.f, 1);
    if(programData.checked < 0)
    {
        errorControl("\nERROR: Can only read file in byte mode! This means that no characters can be analyzed!\n");
        free(programData.filename);
        abort();
    }
    programData.charInfo = malloc(sizeof(letterInfo) * (MAX_DEFAULT_SYMBOLS + 1));
    if(programData.charInfo == NULL)
    {
        errorControl("ERROR: Allocating memory for information on our characters failed!\n");
        free(programData.filename);
        abort();
    }
    verbosing("done!\n", programData.verbose);
    return programData;
}

pdata_t moreDifferentCharacters(pdata_t programData)
{
    programData.maxSymbols = programData.maxSymbols * 2;
    letterInfo *newCharInfo = malloc(sizeof(letterInfo) * (programData.maxSymbols + 1));
    if(newCharInfo == NULL)
    {
        emergencyCleanup(programData, "ERROR: Allocating memory for new information on our characters failed!\n");
    }
    for (int i = 0; i < programData.differentSymbols; i++)
    {
        newCharInfo[i].absFreq = programData.charInfo[i].absFreq;
        for (int j = 0; j < MAX_DESCRIPTION_LENGTH; j++)
        {
            newCharInfo[i].description[j] = programData.charInfo[i].description[j];
        }
        newCharInfo[i].descriptionLength = programData.charInfo[i].descriptionLength;
        newCharInfo[i].character = programData.charInfo[i].character;
    }
    free(programData.charInfo);
    programData.charInfo = newCharInfo;
    return programData;
}

pdata_t countingCharacters(pdata_t programData)
{
    wint_t currentCharacter;
    verbosing("Beginning Counting Characters...\n", programData.verbose);
    do
    {
        currentCharacter = fgetwc(programData.f);
        if(errno == EILSEQ)
        {
            emergencyCleanup(programData, "Wide character conversion failed!\n");
        }
        bool notFound = true;
        for (int i = 0; i < programData.differentSymbols; i++)
        {
            if (currentCharacter == programData.charInfo[i].character)
            {
                programData.charInfo[i].absFreq++;
                notFound = false;
                break;
            }
        }
        if (notFound)
        {
            programData.charInfo[programData.differentSymbols].character = currentCharacter;
            programData.charInfo[programData.differentSymbols].absFreq = 1;
            bool defaultTrigger = false;
            switch (currentCharacter)
            {
                case '\t':
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description, L"TAB");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 3;
                    break;
                case '\n':
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description, L"Newline");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 7;
                    break;
                case ' ':
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description, L"SPACE");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 5;
                    break;
                case WEOF:
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description, L"EOF");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 3;
                    break;
                case '\f':
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description, L"Page Break");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 10;
                    break;
                case '\v':
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description, L"Vertical TAB");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 12;
                    break;
                case '\r':
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description,
                           L"Carriage Return");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 15;
                    break;
                case 768:
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description,
                           L"Combining Grave Accent");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 22;
                    break;
                case 772:
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description,
                           L"Combining Macron");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 16;
                    break;
                case 780:
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description,
                           L"Combining Caron");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 15;
                    break;
                case 65039:
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description,
                           L"Variation Selector-16");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 21;
                    break;
                case 8205:
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description,
                           L"CJK character \'uncle\'");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 21;
                    break;
                case 8419:
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description,
                           L"Japanese symbol \'very tired\'");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 28;
                    break;
                case 917536:
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description, L"TAG SPACE");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 9;
                    break;
                case 917631:
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description, L"Cancel Tag");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 10;
                    break;
                case '\'':
                    wcscpy((wchar_t *) programData.charInfo[programData.differentSymbols].description, L"\"\'\"");
                    programData.charInfo[programData.differentSymbols].descriptionLength = 3;
                    break;
                default:
                    defaultTrigger = true;
            }
            int currentCharacterWidth = wcwidth((wchar_t) currentCharacter);
            if(defaultTrigger)
            {
                if (currentCharacterWidth < 0)
                {
                    if(currentCharacter >= 129648 && currentCharacter <= 129791)
                    {
                        programData.checked = swprintf((wchar_t *) programData.charInfo[programData.differentSymbols].description, sizeof(wchar_t) * MAX_DESCRIPTION_LENGTH, L"Symbols and Pictographs Extended-A No. %d", currentCharacter);
                        programData.charInfo[programData.differentSymbols].descriptionLength = (int) wcslen((wchar_t *) programData.charInfo[programData.differentSymbols].description);
                        defaultTrigger = false;
                    }
                    else if(currentCharacter >= 128640 && currentCharacter <= 1287671)
                    {
                        programData.checked = swprintf((wchar_t *) programData.charInfo[programData.differentSymbols].description, sizeof(wchar_t) * MAX_DESCRIPTION_LENGTH, L"Transport and Map Symbols No. %d", currentCharacter);
                        programData.charInfo[programData.differentSymbols].descriptionLength = (int) wcslen((wchar_t *) programData.charInfo[programData.differentSymbols].description);
                        defaultTrigger = false;
                    }
                    else if((currentCharacter >= 127136 && currentCharacter <= 127231) || (currentCharacter >= 127232 && currentCharacter <= 127486))
                    {
                        programData.checked = swprintf((wchar_t *) programData.charInfo[programData.differentSymbols].description, sizeof(wchar_t) * MAX_DESCRIPTION_LENGTH, L"Playing Cards No. %d", currentCharacter);
                        programData.charInfo[programData.differentSymbols].descriptionLength = (int) wcslen((wchar_t *) programData.charInfo[programData.differentSymbols].description);
                        defaultTrigger = false;
                    }
                    else if(currentCharacter >= 127488 && currentCharacter <= 127743)
                    {
                        programData.checked = swprintf((wchar_t *) programData.charInfo[programData.differentSymbols].description, sizeof(wchar_t) * MAX_DESCRIPTION_LENGTH, L"Enclosed Alphanumeric Supplement No. %d", currentCharacter);
                        programData.charInfo[programData.differentSymbols].descriptionLength = (int) wcslen((wchar_t *) programData.charInfo[programData.differentSymbols].description);
                        defaultTrigger = false;
                    }
                    else
                    {
                        warning("WARNING: Control character found!\nControl characters need to be explained, code needs to be added for that purpose, please open an issue at \"https://codeberg.org/Elshid/LetterDistributionViewer\"!\n",
                                programData.verbose);
                        if (programData.verbose)
                        {
                            printf("The number of the control character is %d\n", currentCharacter);
                        }
                        programData.numUnprintableCharacters++;
                    }
                    if(programData.checked == -1)
                    {
                        emergencyCleanup(programData,"ERROR: Pitting the description of the current character into the character information struct failed!\n");
                    }
                }
            }
            if (defaultTrigger)
            {
                if (currentCharacterWidth == 0)
                {
                    warning("WARNING: Unprintable character found!\nUnprintable characters need to be explained, code needs to be added for that purpose, please open an issue at \"https://codeberg.org/Elshid/LetterDistributionViewer\"!\n",
                            programData.verbose);
                    if (programData.verbose)
                    {
                        printf("The number of the unprintable character is %d\n", currentCharacter);
                    }
                }
                else if(currentCharacterWidth > MAX_DESCRIPTION_LENGTH - 2)
                {
                    warning("WARNING: The length of a unicode character is bigger than the width of the character column in the table, which looks ugly and may lead to SEGFAULTS.\nPlease open an issue at \"https://codeberg.org/Elshid/LetterDistributionViewer\"!\n", programData.verbose);
                    if(programData.verbose)
                    {
                        printf("The with of this unicode character is: %d\n", currentCharacterWidth);
                    }
                }
                programData.charInfo[programData.differentSymbols].description[0] = '\'';
                programData.charInfo[programData.differentSymbols].description[1] = currentCharacter;
                programData.charInfo[programData.differentSymbols].description[2] = '\'';
                programData.charInfo[programData.differentSymbols].descriptionLength = 2 + currentCharacterWidth;
                if(programData.charInfo[programData.differentSymbols].descriptionLength > programData.maxRealDescriptionLength)
                {
                    programData.maxRealDescriptionLength = programData.charInfo[programData.differentSymbols].descriptionLength;
                }
            }
            programData.differentSymbols++;
        }
        if (programData.differentSymbols >= programData.maxSymbols)
        {
            programData = moreDifferentCharacters(programData);
        }
        programData.totalSymbols++;
    } while (currentCharacter != WEOF);
    verbosing("done!\n", programData.verbose);
    return programData;
}

void sortCharacters(letterInfo *charInfo, int differentSymbols, bool verbose)
{
    verbosing("Sorting Characters by number of occurrences...", verbose);
    qsort(charInfo, differentSymbols, sizeof(letterInfo), compareLetterByFreq);
    verbosing("done!\n", verbose);
}

long double calculateEntropy(pdata_t programData)
{
    verbosing("Calculating Entropy...\n", programData.verbose);
    long double entropy = 0;
    for (int i = 0; i < programData.differentSymbols; i++)
    {
        programData.charInfo[i].relFreq = (long double) programData.charInfo[i].absFreq / (long double) programData.totalSymbols;
        entropy += programData.charInfo[i].relFreq * log2((double) programData.charInfo[i].relFreq);
    }
    entropy = entropy * -1;
    verbosing("done!\n", programData.verbose);
    return entropy;
}

void printTableEnd(int part, int maxCharLength)
{
    int length = 1;
    if(part == 0)
    {
        length = length + maxCharLength;
    }
    else
    {
        length = length + 1 + ABSOLUTE_FREQUENCY_LENGTH;
    }
    for(int i = 0; i < length; i++)
    {
        printf("-");
    }
}

void printCharacters(pdata_t programData, long double entropy)
{
    verbosing("Printing all characters and their respective occurrences...", programData.verbose);
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    int hundredPercentLength = w.ws_col - (ABSOLUTE_FREQUENCY_LENGTH * 2) - programData.maxRealDescriptionLength - 9;
    printf("\n| Character ");
    for(int i = 0; i < programData.maxRealDescriptionLength - 10; i++)
    {
        printf(" ");
    }
    printf("| Absolute Frequency | Relative Frequency |\n");
    printf("|");
    printTableEnd(0, programData.maxRealDescriptionLength);
    printf("|");
    printTableEnd(1, 0);
    printf("|");
    printTableEnd(2, 0);
    printf("|\n");
    for (int i = 0; i < programData.differentSymbols; i++)
    {
        char assembly[100];
        printf("| ");
        for(int j = 0; j < programData.charInfo[i].descriptionLength; j++)
        {
            programData.checked = printf("%lc", (wchar_t) programData.charInfo[i].description[j]);
        }
        int spaces = programData.maxRealDescriptionLength - programData.charInfo[i].descriptionLength;
        for(int j = 0; j < spaces; j++)
        {
            printf(" ");
        }
        sprintf(assembly, "%d", programData.charInfo[i].absFreq);
        if(programData.checked < 0)
        {
            emergencyCleanup(programData, "ERROR: Putting the absolute frequency into its array failed!\n");
        }
        printf("| %s ", assembly);
        spaces = ABSOLUTE_FREQUENCY_LENGTH - (int) strlen(assembly);
        for(int j = 0; j < spaces; j++)
        {
            printf(" ");
        }
        programData.checked = sprintf(assembly, "%Lf", programData.charInfo[i].relFreq);
        if(programData.checked < 0)
        {
            emergencyCleanup(programData, "ERROR: Putting the relative frequency into its array failed!\n");
        }
        printf("| %s ", assembly);
        spaces = ABSOLUTE_FREQUENCY_LENGTH - strlen(assembly);
        for(int j = 0; j < spaces; j++)
        {
            printf(" ");
        }
        printf("|");
        if(programData.printDiagram)
        {
            int perMill = (int) (programData.charInfo[i].relFreq * hundredPercentLength);
            for(int j = 0; j < perMill; j++)
            {
                printf("*");
            }
        }
        printf("\n");
    }
    printf("|");
    printTableEnd(0, programData.maxRealDescriptionLength);
    printf("|");
    printTableEnd(1, 0);
    printf("|");
    printTableEnd(2, 0);
    printf("|\n");
    printf("Total Characters: %d\n", programData.totalSymbols);
    printf("Unprintable characters: %d\n", programData.numUnprintableCharacters);
    printf("Entropy of the file: %Lf bit\n", entropy);
    verbosing("done!\n", programData.verbose);
}

int main(int argc, char **argv)
{
    setlocale(LC_ALL, "");
    pdata_t programData = getParameters(argc, argv);
    programData = countingCharacters(programData);
    sortCharacters(programData.charInfo, programData.differentSymbols, programData.verbose);
    long double entropy = calculateEntropy(programData);
    printCharacters(programData, entropy);
    cleanup(programData);
    return EXIT_SUCCESS;
}
